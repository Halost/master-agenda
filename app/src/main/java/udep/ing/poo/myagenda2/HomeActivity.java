package udep.ing.poo.myagenda2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Calendar;

public class HomeActivity extends AppCompatActivity implements CalendarView.OnDateChangeListener{

    private CalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        calendarView = (CalendarView)findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener((CalendarView.OnDateChangeListener) this);
        textView3 = (TextView)findViewById(R.id.textView3);

        Intent i = getIntent();
        Bundle bundle = getIntent().getExtras();
        String inputUsuario = (String)bundle.get("usuario");
        textView3.setText("Agenda de " + inputUsuario);

        final Switch switchDark = findViewById(R.id.switch2);
        SharedPreferences sharedPreferences = getSharedPreferences("save",MODE_PRIVATE);
        switchDark.setChecked(sharedPreferences.getBoolean("value",true));

        if(sharedPreferences.getBoolean("value",true)){
            SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
            editor.putBoolean("value",true);
            editor.apply();
            switchDark.setChecked(true);
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }else{
            SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
            editor.putBoolean("value",false);
            editor.apply();
            switchDark.setChecked(false);
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        switchDark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value",true);
                    editor.apply();
                    switchDark.setChecked(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }else{
                    SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value",false);
                    editor.apply();
                    switchDark.setChecked(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
            }
        });
    }
    TextView textView3;

    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int i, int i1, int i2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        CharSequence []items = new CharSequence[3];
        items[0] = "Agregar Actividad";
        items[1] = "Ver Actividades";
        items[2] = "Cancelar";

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (i==0){
                    Intent intent = new Intent(getApplication(),CrearActividad.class);
                    Bundle bundle = new Bundle();

                    intent.putExtras(bundle);
                    startActivity(intent);

                }else if (i==1){
                    Intent intent = new Intent(getApplication(),ListaActividades.class);
                    Bundle bundle = new Bundle();

                    intent.putExtras(bundle);
                    startActivity(intent);

                }else{
                    return;
                }

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
