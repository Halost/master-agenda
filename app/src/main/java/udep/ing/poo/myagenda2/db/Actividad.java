package udep.ing.poo.myagenda2.db;

public class Actividad {
    private int id;
    private String titulo;
    private String descripcion;
    private String fecha;
    private String hora;

    public Actividad(int id, String titulo, String descripcion, String fecha, String hora) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.hora = hora;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public static final String CREAR_TABLA_ACTIVIDAD = "CREATE TABLE actividades (id INTEGER, titulo TEXT, descripcion TEXT, fecha TEXT, hora TEXT)";
    public static final String DROPEAR_TABLA_ACTIVIDAD  = "DROP TABLE IF EXISTS actividades";

    public static final String TABLA_ACTIVIDADES = "actividades";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_TITULO = "titulo";
    public static final String CAMPO_DESCRIPCION = "descripcion";
    public static final String CAMPO_FECHA = "fecha";
    public static final String CAMPO_HORA = "hora";

}
