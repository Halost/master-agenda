package udep.ing.poo.myagenda2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import udep.ing.poo.myagenda2.db.Actividad;
import udep.ing.poo.myagenda2.db.Conexion;

public class CrearActividad extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_actividad);

        botonGuardar = (Button)findViewById(R.id.button5);
        botonGuardar.setOnClickListener(this);
        botonFecha = (Button)findViewById(R.id.button7);
        botonFecha.setOnClickListener(this);
        botonHora = (Button)findViewById(R.id.button8);
        botonHora.setOnClickListener(this);

        editTitulo = (EditText)findViewById((R.id.editText6));
        editDescripcion = (EditText)findViewById(R.id.editText7);
        editFecha = (EditText)findViewById(R.id.editText8);
        editFecha.setEnabled(false);
        editHora = (EditText)findViewById(R.id.editText9);
        editHora.setEnabled(false);
        editId = (EditText)findViewById(R.id.editText10);
    }

    Button botonGuardar, botonFecha, botonHora;
    EditText editTitulo, editDescripcion, editFecha, editHora, editId;
    private int dia, mes, ano, hora, minutos;

    @Override
    public void onClick(View view) {
        if (view.getId() == botonGuardar.getId()){
            Conexion conexion = new Conexion(this);
            SQLiteDatabase db = conexion.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(Actividad.CAMPO_ID, editId.getText().toString());
            values.put(Actividad.CAMPO_TITULO, editTitulo.getText().toString());
            values.put(Actividad.CAMPO_DESCRIPCION, editDescripcion.getText().toString());
            values.put(Actividad.CAMPO_FECHA, editFecha.getText().toString());
            values.put(Actividad.CAMPO_HORA, editHora.getText().toString());

            Long id = db.insert(Actividad.TABLA_ACTIVIDADES, Actividad.CAMPO_ID,values);


            Toast.makeText(this,"id usuario:"+ id, Toast.LENGTH_LONG).show();

            Intent intent = new Intent(this, ListaActividades.class);
            startActivity(intent);

        }else if (view.getId() == botonFecha.getId()){

            final Calendar c = Calendar.getInstance();
            dia=c.get(Calendar.DAY_OF_MONTH);
            mes=c.get(Calendar.MONTH);
            ano=c.get(Calendar.YEAR);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    editFecha.setText(dayOfMonth+"/"+(month+1)+"/"+year);
                }
            },dia,mes,ano);
            datePickerDialog.show();
        }   if (view.getId() == botonHora.getId()){
            final Calendar c = Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minutos = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    editHora.setText(hourOfDay+":"+minute);
                }
            },hora,minutos,false);
            timePickerDialog.show();
        }
        return;
    }
}
