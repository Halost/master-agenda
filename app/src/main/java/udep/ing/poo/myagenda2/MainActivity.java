package udep.ing.poo.myagenda2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private final String CHANNEL_ID = "personal_notifications"; // PROBLEMA OREO
    private final int NOTIFICATION_ID = 001;// PROBLEMA OREO

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonIniciarSesion = (Button) findViewById(R.id.button);
        botonRegistrarme = (Button) findViewById(R.id.button2);
        editUsuario = (EditText)findViewById((R.id.editText));
        editConstrasena = (EditText)findViewById(R.id.editText2);

        botonIniciarSesion.setOnClickListener(this);
        botonRegistrarme.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle!= null){
            String usuario = (String) bundle.get("usuario");
            editUsuario.setText(usuario);
        }

        final Switch switchDark = findViewById(R.id.switch1);
        SharedPreferences sharedPreferences = getSharedPreferences("save",MODE_PRIVATE);
        switchDark.setChecked(sharedPreferences.getBoolean("value",true));

        if(sharedPreferences.getBoolean("value",true)){
            SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
            editor.putBoolean("value",true);
            editor.apply();
            switchDark.setChecked(true);
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }else{
            SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
            editor.putBoolean("value",false);
            editor.apply();
            switchDark.setChecked(false);
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        switchDark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value",true);
                    editor.apply();
                    switchDark.setChecked(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }else{
                    SharedPreferences.Editor editor = getSharedPreferences("save",MODE_PRIVATE).edit();
                    editor.putBoolean("value",false);
                    editor.apply();
                    switchDark.setChecked(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
            }
        });
    }

    Button botonIniciarSesion, botonRegistrarme;
    EditText editUsuario, editConstrasena;

    private void createNotificationChannel(){// PROBLEMA OREO
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            CharSequence name = "Personal Notifications";
            String description = "Include all the personal notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,name,importance);

            notificationChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button){

            String inputUsuario = editUsuario.getText().toString();
            String inputContrasena = editConstrasena.getText().toString();

            if (inputUsuario.equals("")){
                //usuario está en blanco
                Toast.makeText(this,getString(R.string.alertaUsuarioBlanco),Toast.LENGTH_SHORT).show();
                createNotificationChannel(); //PROBLEMA OREO
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_ID); // PROBLEMA OREO (CHANEL_ID)
                builder.setContentTitle("Aún sin registrarse?");
                builder.setContentText("Pulsa aquí para el registro");
                builder.setSmallIcon(R.drawable.ic_launcher_background);
                builder.setAutoCancel(true);

                NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
                notificationManagerCompat.notify(NOTIFICATION_ID,builder.build());

                Intent intent = new Intent(this, SignupActivity.class);
                PendingIntent pIntent = PendingIntent.getActivity(this,0,intent,0);

                builder.setContentIntent(pIntent);

                NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(1,builder.build());

            }
            else if (inputContrasena.equals("")){
                //contraseña en blanco
                Toast.makeText(this,getString(R.string.alertaContrasenaBlanco),Toast.LENGTH_SHORT).show();
            }
            else {

                Usuarios usuarios = new Usuarios(this.getSharedPreferences("MyAgenda2",Context.MODE_PRIVATE));

                if (usuarios.validar(inputUsuario, inputContrasena)){
                    Intent intent = new Intent(this,HomeActivity.class);
                    intent.putExtra("usuario",inputUsuario);
                    startActivity(intent);
                }else {
                    Toast.makeText(this,getString(R.string.alertaUsuarioContrasena),Toast.LENGTH_SHORT).show();

                    createNotificationChannel(); //PROBLEMA OREO
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_ID); // PROBLEMA OREO (CHANEL_ID)
                    builder.setContentTitle("Aún sin registrarse?");
                    builder.setContentText("Pulsa aquí para el registro");
                    builder.setSmallIcon(R.drawable.ic_launcher_background);
                    builder.setAutoCancel(true);

                    NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
                    notificationManagerCompat.notify(NOTIFICATION_ID,builder.build());

                    Intent intent = new Intent(this, SignupActivity.class);
                    PendingIntent pIntent = PendingIntent.getActivity(this,0,intent,0);

                    builder.setContentIntent(pIntent);

                    NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.notify(1,builder.build());
                }

            }
        }
        else if (v.getId() == R.id.button2){
            Intent intent = new Intent(this, SignupActivity.class);
            startActivity(intent);
        }
    }
}
