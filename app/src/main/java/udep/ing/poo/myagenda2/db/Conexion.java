package udep.ing.poo.myagenda2.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Conexion extends SQLiteOpenHelper {

    public static final String BD_ACTIVIDADES = "bd_actividades";

    public Conexion(@Nullable Context context) {
        super(context, BD_ACTIVIDADES, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Actividad.CREAR_TABLA_ACTIVIDAD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(Actividad.DROPEAR_TABLA_ACTIVIDAD);
        onCreate(sqLiteDatabase);
    }
}
