package udep.ing.poo.myagenda2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import udep.ing.poo.myagenda2.db.Actividad;
import udep.ing.poo.myagenda2.db.Conexion;

public class ListaActividades extends AppCompatActivity implements AdapterView.OnItemClickListener {

    Conexion conexion;
    ArrayAdapter<String> adaptador;
    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_actividades);

        conexion = new Conexion(this);

        lista = (ListView)findViewById(R.id.ListView);
        lista.setOnItemClickListener(this);

        List<String> datoslista = getLista();

        adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, datoslista);
        lista.setAdapter(adaptador);
    }
    List<Actividad> listaActividades;

    private List<String> getLista(){
        listaActividades = new LinkedList<Actividad>();

        List<String> listaTitulos = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Actividad.CAMPO_ID, Actividad.CAMPO_TITULO, Actividad.CAMPO_DESCRIPCION,Actividad.CAMPO_FECHA,Actividad.CAMPO_HORA};

        Cursor cursor = db.query(Actividad.TABLA_ACTIVIDADES, campos, null,null,null,null, null);
        while (cursor.moveToNext()){

            int id = cursor.getInt(0);
            String titulo = cursor.getString(1);
            String descripcion = cursor.getString(2);
            String fecha = cursor.getString(3);
            String hora = cursor.getString(4);

            Actividad a = new Actividad(id,titulo,descripcion,fecha,hora);

            listaActividades.add(a);

            listaTitulos.add(a.getTitulo());

        }

        return listaTitulos;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

    Intent intent = new Intent(this,ModificarActividad.class);

    intent.putExtra(Actividad.CAMPO_ID, listaActividades.get(i).getId());
    intent.putExtra(Actividad.CAMPO_TITULO, listaActividades.get(i).getTitulo());
    intent.putExtra(Actividad.CAMPO_DESCRIPCION, listaActividades.get(i).getDescripcion());
    intent.putExtra(Actividad.CAMPO_FECHA, listaActividades.get(i).getFecha());
    intent.putExtra(Actividad.CAMPO_HORA, listaActividades.get(i).getHora());

    startActivity(intent);

    }
}
