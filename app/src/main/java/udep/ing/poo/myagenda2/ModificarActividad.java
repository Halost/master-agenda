package udep.ing.poo.myagenda2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

import udep.ing.poo.myagenda2.db.Actividad;
import udep.ing.poo.myagenda2.db.Conexion;

public class ModificarActividad extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_actividad);

        conexion = new Conexion(this);

        botonEliminar = (Button)findViewById(R.id.button6);
        botonEliminar.setOnClickListener(this);
        botonModificar = (Button)findViewById(R.id.button4);
        botonModificar.setOnClickListener(this);

        botonFechaM = (Button)findViewById(R.id.button9);
        botonFechaM.setOnClickListener(this);
        botonHoraM = (Button)findViewById(R.id.button10);
        botonHoraM.setOnClickListener(this);

        editTituloM = (EditText)findViewById((R.id.editText11));
        editDescripcionM = (EditText)findViewById(R.id.editText12);
        editFechaM = (EditText)findViewById(R.id.editText13);
        editFechaM.setEnabled(false);
        editHoraM = (EditText)findViewById(R.id.editText14);
        editHoraM.setEnabled(false);
        editIdM = (EditText)findViewById(R.id.editText15);
        editIdM.setEnabled(false);

        Bundle extras = getIntent().getExtras();

        int idActividad = extras.getInt(Actividad.CAMPO_ID,0);
        String titulo = extras.getString(Actividad.CAMPO_TITULO,"");
        String descripcion = extras.getString(Actividad.CAMPO_DESCRIPCION,"");
        String fecha = extras.getString(Actividad.CAMPO_FECHA,"");
        String hora = extras.getString(Actividad.CAMPO_HORA,"");

        editIdM.setText(idActividad+"");
        editTituloM.setText(titulo);
        editDescripcionM.setText(descripcion);
        editFechaM.setText(fecha);
        editHoraM.setText(hora);
    }

    Button botonEliminar, botonModificar, botonFechaM, botonHoraM;
    EditText editTituloM, editDescripcionM, editFechaM, editHoraM, editIdM;
    Conexion conexion ;
    private int dia, mes, ano, hora, minutos;

    @Override
    public void onClick(View view) {

        SQLiteDatabase db = conexion.getWritableDatabase();
        String[] idTitulo = {editIdM.getText().toString()};

        if (view.getId() == botonModificar.getId()){

            ContentValues values = new ContentValues();
            values.put(Actividad.CAMPO_TITULO, editTituloM.getText().toString());
            values.put(Actividad.CAMPO_DESCRIPCION, editDescripcionM.getText().toString());
            values.put(Actividad.CAMPO_FECHA, editFechaM.getText().toString());
            values.put(Actividad.CAMPO_HORA, editHoraM.getText().toString());

            db.update(Actividad.TABLA_ACTIVIDADES, values,Actividad.CAMPO_ID+"=?",idTitulo);

            Intent intent = new Intent(this, ListaActividades.class);
            startActivity(intent);

        }else if (view.getId() == botonEliminar.getId()){

            db.delete(Actividad.TABLA_ACTIVIDADES,Actividad.CAMPO_ID+"=?", idTitulo );

            Intent intent = new Intent(this, ListaActividades.class);
            startActivity(intent);
        }else if (view.getId() == botonFechaM.getId()){

            final Calendar c = Calendar.getInstance();
            dia=c.get(Calendar.DAY_OF_MONTH);
            mes=c.get(Calendar.MONTH);
            ano=c.get(Calendar.YEAR);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    editFechaM.setText(dayOfMonth+"/"+(month+1)+"/"+year);
                }
            },dia,mes,ano);
            datePickerDialog.show();
        }   if (view.getId() == botonHoraM.getId()){
            final Calendar c = Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minutos = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    editHoraM.setText(hourOfDay+":"+minute);
                }
            },hora,minutos,false);
            timePickerDialog.show();
        }
        db.close();
    }
}
